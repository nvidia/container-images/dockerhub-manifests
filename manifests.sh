#!/bin/bash

set -eu
set -o pipefail

if [ "$#" -ne 2 ]; then
    echo "ERROR: Invalid arguments" 1>&2
    echo "usage: ./$0 REPOSITORY ARCHS" 1>&2
    exit 1
fi

for bin in curl jq docker; do
    if ! which $bin >/dev/null 2>&1; then
	echo "ERROR: Missing required tool: $bin" 1>&2
	exit 1
    fi
done

if ! docker manifest >/dev/null 2>&1; then
    echo "ERROR: \"docker manifest\" is not supported" 1>&2
    exit 1
fi

REPOSITORY="$1"
ARCHS="$2"
ALL_TAGS=""

for ARCH in ${ARCHS}; do
    ARCH_REPOSITORY="${REPOSITORY}-${ARCH}";
    echo "${ARCH_REPOSITORY}"

    TOKEN=$(curl -fsSL "https://auth.docker.io/token?service=registry.docker.io&scope=repository:${ARCH_REPOSITORY}:pull" | jq -r '.token');
    TAGS=$(curl -fsSL -H "Authorization: Bearer $TOKEN" "https://index.docker.io/v2/${ARCH_REPOSITORY}/tags/list" | jq -r '.tags[]');
    ALL_TAGS="${ALL_TAGS} ${TAGS}"

    for TAG in ${TAGS}; do
	docker manifest create --amend "${REPOSITORY}:${TAG}" "${ARCH_REPOSITORY}:${TAG}";
        docker manifest annotate "${REPOSITORY}:${TAG}" "${ARCH_REPOSITORY}:${TAG}" --arch="${ARCH}";
    done
    echo
done

ALL_TAGS=$(echo "${ALL_TAGS}" | tr ' ' '\n' | sort -u)
for TAG in ${ALL_TAGS}; do
    echo "${REPOSITORY}:${TAG}"
    docker manifest push --purge "${REPOSITORY}:${TAG}";
    echo
done
